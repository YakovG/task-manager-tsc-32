package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void addAll(Collection<E> collection);

    @Nullable
    E add(E entity);

    @Nullable
    List<E> findAll();

    @Nullable
    E findOneById(String id);

    @Nullable
    E findOneByIndex(Integer index);

    boolean isAbsentById(String id);

    boolean isAbsentByIndex(Integer index);

    String getIdByIndex(Integer index);

    int size();

    void clear();

    void remove(E entity);

    @Nullable
    E removeOneById(String id);

    @Nullable
    E removeOneByIndex(Integer index);

}
