package ru.goloshchapov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.component.Backup;
import ru.goloshchapov.tm.component.FileScanner;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.system.UnknownArgumentException;
import ru.goloshchapov.tm.exception.system.UnknownCommandException;
import ru.goloshchapov.tm.repository.CommandRepository;
import ru.goloshchapov.tm.repository.ProjectRepository;
import ru.goloshchapov.tm.repository.TaskRepository;
import ru.goloshchapov.tm.repository.UserRepository;
import ru.goloshchapov.tm.service.*;
import ru.goloshchapov.tm.util.SystemUtil;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class Bootstrap implements ServiceLocator {


    @NotNull private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        declareCommands(commandService.getCommandList());
    }

    private void declareCommands (@NotNull List<AbstractCommand> commands) {
        for (@NotNull AbstractCommand command : commands) {
            registry(command);
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = arguments.get(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        try {
            parseArg(arg);
        } catch (final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    private void checkPreliminary() {
        initPID();
        final Predicate<String> checkDefault = "y"::equals;
        System.out.println("USE BACKUP(IF AVAILABLE)? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            backup.load();
            return;
        }
        System.out.println("DELETE BACKUP(IF AVAILABLE)? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            backup.delete();
        }
        System.out.println("CREATE DEFAULT USERS? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            parseCommand("user-create-by-default");
            loggerService.info("DEFAULT USERS CREATED");
        }
        System.out.println("USE TEST DATASET? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            parseCommand("create-test-data");
            loggerService.info("TEST DATASET CREATED");
        }
    }

    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void run(String... args) {
        final Runnable messageFail = () -> System.err.println("[FAIL]");
        final Runnable messageOk = () -> System.out.println("[OK]");
        final Runnable messageCommand = () -> System.out.println("[ENTER COMMAND]");
        loggerService.debug("DEBUG INFO!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        checkPreliminary();
        backup.init();
        fileScanner.init();
        messageCommand.run();
        while (true) {
            @Nullable final String cmd = TerminalUtil.nextLine();
            loggerService.command(cmd);
            try {
                parseCommand(cmd);
                messageOk.run();
            } catch (final Exception e) {
                loggerService.error(e);
                messageFail.run();
                messageCommand.run();
            }
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

}
