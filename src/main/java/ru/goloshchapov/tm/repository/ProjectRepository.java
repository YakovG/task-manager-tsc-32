package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
