package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull public static final String NAME = "task-list";

    @NotNull public static final String DESCRIPTION = "Show task list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
