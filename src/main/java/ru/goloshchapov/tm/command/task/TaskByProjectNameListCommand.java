package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectNameListCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-list-by-project-name";

    @NotNull public static final String DESCRIPTION = "Show task list by project name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().findAllByProjectName(userId, name);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
