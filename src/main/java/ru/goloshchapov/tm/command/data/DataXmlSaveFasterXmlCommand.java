package ru.goloshchapov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Domain;
import ru.goloshchapov.tm.enumerated.Role;

import java.io.FileOutputStream;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-xml-fasterxml-save";

    @NotNull public static final String DESCRIPTION = "Save data to XML (FASTERXML)";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @Nullable final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
