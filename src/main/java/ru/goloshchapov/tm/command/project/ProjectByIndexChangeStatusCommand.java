package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.ProjectNotUpdatedException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIndexChangeStatusCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-change-status-by-index";

    @NotNull public static final String DESCRIPTION ="Change project status by index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER STATUS:");
        @NotNull final Status[] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        @Nullable final String statusChange = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().changeOneStatusByIndex(userId, index, statusChange);
        if (project == null) throw new ProjectNotUpdatedException();
    }
}
